require("@nomicfoundation/hardhat-toolbox");

require('dotenv').config()

require("@nomiclabs/hardhat-solhint");

//require('@openzeppelin/hardhat-upgrades');

/** @type import('hardhat/config').HardhatUserConfig */
module.exports = {
  solidity: {
    compilers: [
      {
        version: "0.8.10",
        settings: {
          optimizer: {
            enabled: true,
            runs: 200
          }
        }
      },
    ],
  },
  defaultNetwork: "hardhat",
  networks: {
    hardhat: {
    },
    testnet: {
      url: "https://data-seed-prebsc-1-s1.binance.org:8545",
      accounts: [ process.env.WALLET ],
    },
    mainnet: {
      url: "https://data-seed-prebsc-1-s1.binance.org:8545",
      accounts: [ process.env.WALLET ],
    },
  },
  etherscan: {
    apiKey: process.env.ETHSCAN_API
  },
  gasReporter: {
    enabled: true
  }
};
