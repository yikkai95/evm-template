# OXBULL Solidity ContracT Template


### Dependencies

- pip3 install slither-analyzer
- pip3 install solc-select

### Feature

- Contract Testing
- Contract Test Coverage
- [Security Checking](https://github.com/crytic/slither)
- [Prettier](https://github.com/prettier-solidity/prettier-plugin-solidity)
- [Gas Reporter](https://github.com/cgewecke/hardhat-gas-reporter)

### Usage

```sh
# compile
make build

# audit
make audit

# test and gas report
make test

# coverage
make coverage

# prettier, lint, audit, compile
make 
```

### Development

- git clone
- update .env file
- set WALLET variable under repo/settings/CICD
- update name under packages.json
- prepare contract and test
- run `make`
- fix contract and test
- git commit and push

### CICD

- CICD is run every commit
- only contract testing and coverage is run during CICD
- to debug gitlab CICD use `gitlab-runner exec docker test --env WALLET=<DEPLOYER PRIVATE KEY>`
