all: prettier lint audit

build:
	yarn hardhat compile

.PHONY: test
test:
	yarn hardhat test

.PHONY: coverage
coverage:
	yarn hardhat coverage

deploy:
	yarn hardhat deploy --network testnet

lint:
	yarn hardhat check

audit:
	#solc-select install 0.8.10
	solc-select use 0.8.10
	slither . --print human-summary

flatten:
	yarn hardhat flatten > flatten.sol

prettier:
	yarn prettier --write contracts
